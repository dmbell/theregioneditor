package region_editor.controller;

import java.util.Iterator;
import javafx.scene.control.TreeItem;
import region_editor.view.RegionEditorView;
import world_data.Region;
import world_data.WorldDataManager;
import region_editor.view.ConfirmDialog;
import static region_editor.view.ConfirmDialog.CANCEL;
import static region_editor.view.ConfirmDialog.OK;
import static region_editor.RegionEditorConstants.PROMPT_DUPLICATE_ID;
import static region_editor.RegionEditorConstants.PROMPT_ENTER_NEW_REGION_ID;
import static region_editor.RegionEditorConstants.TITLE_ENTER_NEW_REGION_ID;
import world_data.RegionType;
import region_editor.view.InputDialog;
import region_editor.view.MessageDialog;

/**
 * This class provides services for all clicks of the Region Toolbar buttons.
 * @author Douglas Bell
 */
public class RegionEditorRegionToolbarController 
{
   
    // THIS IS THE VIEW THAT THIS REGION CONTROLLERS NEEDS TO USE
    private RegionEditorView view;
    
    //THIS IS THE WORLD DATA STRUCTURE WE NEED ACCESS TO
    private WorldDataManager worldDM;
    
    //REGION VARIABLES WE WILL NEED
    private Region region;
    private Region parentRegion;
    private Region nextSelected;
    private Region testRegion;
    
    //THE TREE VALUE OF THIS REGION
    TreeItem<Region> treeRegion;
    
    //ID NAME INPUT BY USER
    private String idName;
    private String worldName;
            
    //CONTEXT FOR THE UPDATE BUTTON
    public boolean addButtonPressed = false;
    
    //DIALOGS!
    ConfirmDialog confirmDialog;
    MessageDialog messageDialog;
    InputDialog inputDialog;
    
    //STRING CONSTANTS
    private static final String CONFIRM_TITLE = "Confirm Decision";
    private static final String REMOVE_ERROR_MESSAGE = "Illegal Removal - World Node Cannot Be Deleted";
    private static final String NULL_NODE_MESSAGE = "There Are No Regions To Delete";
    private static final String ENTER_WORLD_NAME_ID = "Enter World Name";
    private static final String ENTER_WORLD_NAME_PROMPT = "What is the name of your new world?";
    private static final String NO_PARENT_SELECTED = "You Must Select A Parent For Your New Region";
    private static final String INVALID_REGION_NAME = "You Must Enter A Name";
    private static final String MUST_ENTER_ID = "You Must Enter An ID";
    
    /**
    * This default constructor needs a RegionEditorView reference and a
    * WorldDataManager construct to modify.
    * 
    * @param initView The view this file manager will be providing
    * services to.
    */
    public RegionEditorRegionToolbarController(RegionEditorView initView)
    {
        // KEEP THE SET THE INFO WE NEED
        view = initView;
        worldDM = view.getWorldDataManager();
        
        //INIT DIALOGS
        messageDialog = new MessageDialog(view.getWindow());
	confirmDialog = new ConfirmDialog(view.getWindow());
        inputDialog = new InputDialog(view.getWindow(), messageDialog);
    }
    
    /**
     * Provides proper response to the region editing button.
     */
    public void enableRegionEditing(){
        if (view.getSelectedRegion() != null)
            view.enableRegionEditing(true);
    }
   
    
    /**
     * Provides response to the add region button.  Prompts the user for an ID,
     * determines if it's valid, and sets the proper context for what the user
     * has entered.
     */
    public void addRegion(){
        
        //prompt user for a region ID and enter editing mode, exit if it's false
        if (verifyUniqueID())
        {
            //if the user is starting a new world, prompt name and add
            if (worldDM.getWorld() == null)
            {
                worldName = inputDialog.showInputDialog(ENTER_WORLD_NAME_ID, ENTER_WORLD_NAME_PROMPT);
                
                //exit if the user didn't enter a name
                if (worldName.trim().length() == 0)
                {
                    messageDialog.show(INVALID_REGION_NAME);
                }
                else
                {
                region = new Region(idName, worldName, RegionType.WORLD);
                worldDM.addRegion(region);
                worldDM.setRoot(region);
                view.initWorldTree();
                view.selectRegion(region);
                view.displayRegion(region);
                view.enableRegionEditing(false);
                }
            }
            //if adding a child, default to this view
            else
            {   
                //make sure the user has picked a parent node first!
                if (view.getSelectedRegion() == null)
                {
                    messageDialog.show(NO_PARENT_SELECTED);
                }
                //move on if they're cool
                else
                {
                addButtonPressed = true;
                enableRegionEditing();
                view.newRegionFieldChangeCHILD(idName);  
                }
            }
        }
        //display the proper message
        else
        {   
            if (idName.trim().length() == 0)
                messageDialog.show(MUST_ENTER_ID);
            else
                messageDialog.show(PROMPT_DUPLICATE_ID);
        }
    }
    
    /**
     * Provides response to the remove region button.  Confirms if the user 
     * really wants to delete the region.  Also prevents user from the deleting 
     * the world region.
     * 
     * @return true or false, depending on if the removal was successful or not. 
     */
    public boolean removeRegion()
    {
        //DISPLAY AN ERROR IF THERE'S NOTHING TO REMOVE
        if (view.getSelectedRegion() == null)
        {
            messageDialog.show(NULL_NODE_MESSAGE);
            return false;
        }
        //INIT THE REGION AND ITS TREEITEM
        region = view.getSelectedRegion();
        parentRegion = region.getParentRegion();
        treeRegion = view.getRegionNode(region);
        
        //CHECK IF THE USER IS TRYING TO DELETE THE WORLD AND GIVE THEM AN ERROR
        if (region.equals(view.getWorldRoot())){
            messageDialog.show(REMOVE_ERROR_MESSAGE);
            return false;
        }
        //CONFIRM THE USER WANTS TO DELETE THE REGION
        else
        {
            String selection = CANCEL;
            
            selection = confirmDialog.showOkCancel(
			CONFIRM_TITLE, 
                        "Are you sure to want to delete " + region.getName() + "?");
            
           //DELETE THE REGION AND CHECK FOR NEXT NODE TO DISPLAY
           if (selection == OK)
           {
                //if the node we're deleting has children, delete them first.
                if (region.hasSubRegions()){

                    Iterator iter = region.getSubRegions();     
                    removeSubRegions(region);
                }
               
                
                if (treeRegion.previousSibling() != null)
                {
                    nextSelected = treeRegion.previousSibling().getValue();
                    worldDM.removeRegion(region);
                    parentRegion.removeSubRegion(region);
                    view.initWorldTree();
                    view.selectRegion(nextSelected);
                    view.refreshWorldTree(nextSelected);
                    view.displayRegion(nextSelected);
                }
                else if (treeRegion.nextSibling() != null)
                {
                    nextSelected = treeRegion.nextSibling().getValue();
                    worldDM.removeRegion(region);
                    parentRegion.removeSubRegion(region);
                    view.initWorldTree();
                    view.selectRegion(nextSelected);
                    view.refreshWorldTree(nextSelected);
                    view.displayRegion(nextSelected);
                }
                else{
                    nextSelected = treeRegion.getParent().getValue();
                    worldDM.removeRegion(region);
                    parentRegion.removeSubRegion(region);
                    view.initWorldTree();
                    view.selectRegion(nextSelected);
                    view.refreshWorldTree(nextSelected);
                    view.displayRegion(nextSelected);
                }     
                return true;
            }
            //return false if the user changes their mind
            else
               return false;
        }      
    }
    
    /**
     * Provides response to the update button, whose actions are determined by
     * the add button context it is in.
     * 
     * @return True or false, depending on if it's successful or not. 
     */
    public boolean update()
    {
        //UPDATE CONTEXT - ADDING TO A PARENT
        if (addButtonPressed)
        {   
            //check if the user put in an invalid region name
            if (view.getInputName().trim().length() == 0)
            {
                messageDialog.show(INVALID_REGION_NAME);
                view.resetNameInput();
                return false;
            }
            else
            {
            //make the new region
            region = new Region(idName, view.getInputName(), 
                    view.getInputType(), view.getInputCapital());
            //get the parentRegion
            parentRegion = view.getSelectedRegion();
            //add the region to the worldDM
            worldDM.addRegion(region, parentRegion);
            worldDM.addRegion(region);
            //refresh and display the region
            view.initWorldTree();
            view.selectRegion(region);
            view.displayRegion(region);
            addButtonPressed = false;
            return true;
            }
        }
        //UPDATE CONTEXT - EDITING A REGION
        else 
        {
            region = view.getSelectedRegion();
            
            //first test to see if the id has been changed
            if (!view.getInputId().equals(region.getId()))
            {
                //if it has been changed, check if its a dupe or not
                idName = view.getInputId();
                testRegion = new Region(idName, "", RegionType.WORLD);

                //show error and reset the idField if invalid
                if (worldDM.hasRegion(testRegion))
                {
                    messageDialog.show(PROMPT_DUPLICATE_ID);
                    view.resetIdInput();
                    return false;         
                }
            }
            
            //check if the user put in an invalid region name
            if (view.getInputName().trim().length() == 0)
            {
                messageDialog.show(INVALID_REGION_NAME);
                view.resetNameInput();
                return false;
            }
            else
            //if the name and id are cool we can set them now
            {    
                region.setId(view.getInputId());
                region.setCapital(view.getInputCapital());
                region.setName(view.getInputName());
                region.setType(view.getInputType());
                view.initWorldTree();
                view.selectRegion(region); 
                return true;
            }
        } 
    }
    
    /**
     * Prompts the user for an ID and determines if it's valid or not.
     * 
     * @return True or false, depending on if it's valid or not.
     */
    public boolean verifyUniqueID()
    {
        //ASK THE USER TO INPUT AN ID
        idName = inputDialog.showInputDialog(TITLE_ENTER_NEW_REGION_ID, PROMPT_ENTER_NEW_REGION_ID);

        //return false right away if the user put nothing in
        if (idName.trim().length() == 0)
            return false;

        //return true right away if this is a new world
        if (worldDM.getWorld() == null)
            return true;         
        
        //if this is an existing world, check if the id exists already or not.
        else 
        {
            //set the idname in a test region to check if it exists
            testRegion = new Region(idName, "test", RegionType.NATION);
            
            //check if this ID exists in the world
            if (worldDM.hasRegion(testRegion))
            {
                testRegion = null;
                return false;
            }
            else
            {
                testRegion = null;
                return true;
            }
        }
    }
    
    
    /**
     * Recursive method to remove all children and their children from
     * the region that is being deleted.
     * 
     * @param region region that is being removed from the true.
     */
    public void removeSubRegions(Region region)
    {
        Iterator iter = region.getSubRegions();
        while (iter.hasNext())
        {
            Region subRegionToRemove = (Region) iter.next();
            if (subRegionToRemove.hasSubRegions())
            {
                removeSubRegions(subRegionToRemove);
            }
            iter.remove();
            worldDM.removeRegion(subRegionToRemove);
        }
    }
};
